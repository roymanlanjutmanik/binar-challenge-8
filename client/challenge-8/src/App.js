import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import Form from "./component/Form/Form";
import Navigation from "./partials/Navigation";
import Home from "./pages/Home";
import FormRegister from "./component/Form/FormRegister/FormRegister";
import FormEdit from "./component/Form/FormEdit/FormEdit";
import FormSearch from "./component/Form/FormSearch/FormSearch";

function App() {
  return (
    <>
      <Router>
        <Navigation />
        <div className="App" >
          <Switch>
            <Route path="/" exact component={Home}></Route>
            <Route path="/register" exact component={FormRegister}></Route>
            <Route path="/edit" exact component={FormEdit}></Route>
            <Route path="/search" exact component={FormSearch}></Route>
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
