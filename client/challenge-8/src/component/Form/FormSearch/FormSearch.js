import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
export default function FormSearch(props) {
  return (
    <div className="row justify-content-center">
      <div className="col-sm-3 my-5 justify-content-center">
        <h5>Search Player</h5>
        <Form method="get" action="">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            name="email"
            id="email"
            placeholder="example@gmail.com"
          ></Form.Control>
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            name="username"
            id="username"
            placeholder="Username"
          ></Form.Control>
          <Form.Label>Experience</Form.Label>
          <Form.Control
            type="text"
            name="experience"
            id="experience"
            placeholder="Experience"
          ></Form.Control>
          <Form.Label>Level</Form.Label>
          <Form.Control
            type="text"
            name="level"
            id="level"
            placeholder="Level"
          ></Form.Control>
        </Form>
        <br />
        <Button variant="success" type="submit">
          Search
        </Button>
      </div>
    </div>
  );
}
