import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default function FormRegister(props) {
  return (
    <div className="row justify-content-center">
      <div className="col-sm-3 my-5 justify-content-center">
        <h5>Register</h5>
        <Form.Group>
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            name="email"
            id="email"
            placeholder="example@gmail.com"
          ></Form.Control>
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            name="username"
            id="username"
            placeholder="Your Username"
          ></Form.Control>
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            id="password"
            placeholder="Your Password"
          ></Form.Control>
        </Form.Group>
        <br />
        <Button variant="success" type="submit">
          Register
        </Button>
      </div>
    </div>
  );
}
