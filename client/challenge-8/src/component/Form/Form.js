// - Buat tampilan/UI untuk fitur berikut :

// - [ ] Form untuk membuat player baru, dengan tombol Submit
// - [ ] Form untuk mengedit player, dengan tombol Submit
// - [ ] Form untuk pencarian player berdasarkan 4 kriteria pencarian:
//  username, email, experience, dan lvl.
// jangan lupa juga tombol submit.

//semua logic di sini

import React, { Component } from "react";
import FormSearch from "./FormSearch/FormSearch";
import FormRegister from "./FormRegister/FormRegister";
import FormEdit from "./FormEdit/FormEdit";

export default class Form extends Component {
  render() {
    return (
      <div className=" container-fluid my-4 ">
        <div className="row">
          <div className="col-sm">
            <FormSearch />
          </div>
          <div className="col-sm">
            <FormRegister />
          </div>
          <div className="col-sm">
            <FormEdit />
          </div>
        </div>
      </div>
    );
  }
}
